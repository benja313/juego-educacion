﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ejercicio2Script : MonoBehaviour {



	public GameObject[] botones  = new GameObject [8];
	private BotonGuitarraScript [] buttonScripts = new BotonGuitarraScript [8];

 // vidas y gamover 
	public int siguiente ;

	  public GameObject gameOver;
    public GameObject win;
    public GameObject vidaOB;

    public float vida;

    private GameObject GM1;
    private GameObject GM2;
    private GameObject GM3;
	// Use this for initialization

    int random;
    	private int valorMax = 9;
	private int valorMin = 1;

    public Text textRandom;
	void Start () {

    this.vida = 3;
    GM1 = GameObject.Find("vida1");
    GM2 = GameObject.Find("vida2");
    GM3 = GameObject.Find("vida3");

		
    random =  generarRandom();

    textRandom.text = random.ToString();
		addScritBoton();

		
	}
	
	// Update is called once per frame
	void Update () {

	}

	private void addScritBoton () {
		for (int i = 0 ; i < 8 ; i++){
			buttonScripts[i] = botones[i].GetComponent<BotonGuitarraScript>();
		}
	}


	public void resultado () {
		Debug.Log("random " + random);
		Debug.Log("colores " + obtenerCantidadPintados());
		if (vida > 0){
					if (random == obtenerCantidadPintados()) {
			Debug.Log("ganaste");
			StartCoroutine(correcto());
		}else {
			Debug.Log("perdiste");
			setVida ();
		}
		}

	}

	private int obtenerCantidadPintados () {

		int contador = 0;
		for (int i = 0 ; i < 8 ; i++){
			if (!buttonScripts[i].getvariable()) {
				contador++;
			}
		}

		return contador;
	}


	    public void setVida () {
        this.vida--;
        StartCoroutine(Explode());
        eliminarVidas();
        
    }

private void eliminarVidas  () {
        if (this.vida == 2) {
            Destroy(GM3);
        } else if (this.vida == 1) {
            Destroy(GM2);
        } else  {
            Destroy(GM1);
            StartCoroutine(finalizar());
            
        }
    }

    IEnumerator Explode()
    {
       this.vidaOB.SetActive(true);
       yield return new WaitForSeconds(0.9f);
       this.vidaOB.SetActive(false);

            
    }

    IEnumerator finalizar()
    {

       yield return new WaitForSeconds(0.9f);
       this.gameOver.SetActive(true);

            
    }      
    

            IEnumerator correcto()
    {
    		this.win.SetActive(true);
       yield return new WaitForSeconds(2.0f);
       SceneManager.LoadScene(siguiente);

            
    }


   private int  generarRandom () {
		return Random.Range (valorMin, valorMax);
	}

	//permite validar que no se retire el menu de gameover
	private void restringirGameOver () {
		if(vida == 0) {
			this.gameOver.SetActive(true);
		}
	}
}
