﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Ejercicio3Script : MonoBehaviour {

		public InputBotonesScripts inputBotonesScripts;
		public ObjetosRandomScript objetosRandomScript;

 // vidas y gamover 

	  public GameObject gameOver;
    public GameObject win;
    public GameObject vidaOB;

    public float vida;

    private GameObject GM1;
    private GameObject GM2;
    private GameObject GM3;
	// Use this for initialization

public int siguiente ;

	
	void Start () {

    this.vida = 3;
    GM1 = GameObject.Find("vida1");
    GM2 = GameObject.Find("vida2");
    GM3 = GameObject.Find("vida3");

		
	}
	
	// Update is called once per frame
	void Update () {
		
	}




	public void resultado () {
		Debug.Log(" " + this.objetosRandomScript.getNumeroRandom());
		Debug.Log(" " + inputBotonesScripts.getValor());


		if (inputBotonesScripts.getValor() == this.objetosRandomScript.getNumeroRandom()) {
			Debug.Log("ganaste");
			StartCoroutine(correcto());
      
		}else {
			Debug.Log("perdiste");
			setVida ();
		}
	}




	    public void setVida () {
        this.vida--;
        StartCoroutine(Explode());
        eliminarVidas();
        
    }

private void eliminarVidas  () {
        if (this.vida == 2) {
            Destroy(GM3);
        } else if (this.vida == 1) {
            Destroy(GM2);
        } else  {
            Destroy(GM1);
            StartCoroutine(finalizar());
            
        }
    }

    IEnumerator Explode()
    {
       this.vidaOB.SetActive(true);
       yield return new WaitForSeconds(0.9f);
       this.vidaOB.SetActive(false);

            
    }

    IEnumerator finalizar()
    {

       yield return new WaitForSeconds(0.9f);
       this.gameOver.SetActive(true);

            
    }


 IEnumerator correcto()
    {
    		this.win.SetActive(true);
       yield return new WaitForSeconds(2.0f);
       SceneManager.LoadScene(siguiente);

            
    }






}
